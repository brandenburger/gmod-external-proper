#pragma once
#include <windows.h>
#include <string>

class c_utils
{
public:
	std::string get_filepath_from_handle(HANDLE process_handle);
};

extern c_utils utils;
