#include "utils.h"

#include <Psapi.h> //GetModuleFileNameEx
#pragma comment( lib, "psapi.lib" )

c_utils utils;

std::string c_utils::get_filepath_from_handle(HANDLE process_handle) {
	char file_path[MAX_PATH + 1];
	GetModuleFileNameEx(process_handle, nullptr, file_path, MAX_PATH);
	return file_path;
}
